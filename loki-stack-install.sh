#!/bin/bash
helm upgrade --install --create-namespace --namespace monitoring loki-stack loki/loki-stack --values=loki-stack-values.yaml
