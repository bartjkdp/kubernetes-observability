#!/bin/bash
helm upgrade --install --create-namespace --namespace monitoring prometheus-operator stable/prometheus-operator --values=prometheus-operator-values.yaml
