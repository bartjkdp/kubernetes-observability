# Kubernetes observability

![Screenshot of the Grafana dashboard with nginx-ingress metrics](/uploads/86d0e642838a7dfe93e3a589f8fadb6b/image.png)

Install Loki and Prometheus operator with:

```bash
./loki-stack-install.sh
./prometheus-operator-install.sh
```

Get an ingress controller:

```bash
./nginx-ingress-install.sh
```

or

```bash
./traefik-install.sh
```

Login on grafana.test with admin / prom-operator

Install test application with:

```bash
./hallo-wereld-install.sh
```

Make some requests with:

```bash
ab -n 10000 http://hallo-wereld.test/
```
