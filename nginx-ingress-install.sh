#!/bin/bash
helm upgrade --install --create-namespace --namespace nginx-ingress nginx-ingress stable/nginx-ingress --values=nginx-ingress-values.yaml
